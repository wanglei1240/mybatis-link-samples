# 简介

[Mybatis Link](https://easy4use.cn)（简称ML）是一个[Mybatis Plus](https://baomidou.com/)（简称MP）的增强工具，在Mybatis Plus的基础上进行了增强，主要解决一对一、一对多等多表联查，以及允许分布式应用远程调用和优化问题，从而达到敏捷开发的目的，实现零SQL编写。

## 特性

- **无侵入:** 在`Mybatis Plus`基础上做了增强，继承了强大的MP特性，配置方式也是一样。
- **CRUD操作:** 通过改造升级，内置通用 Dao、通用 Mgr，实现表单基础的CRUD操作。
- **多表联查操作:** 内置了注解@Link类，用在 Dao 方法上，可以实现一对一连表查询（包括左连接、右连接、内连接），一对多连表查询。

## 代码托管

> **[Gitee](https://gitee.com/easy4use/mybatis-link)**

## 参与贡献

欢迎各路好汉一起来参与完善 MyBatis-Link，我们期待你的 PR！

- 贡献代码：代码地址 [MyBatis-Link](https://gitee.com/easy4use/mybatis-link) ，欢迎提交 Issue 或者 Pull Requests
