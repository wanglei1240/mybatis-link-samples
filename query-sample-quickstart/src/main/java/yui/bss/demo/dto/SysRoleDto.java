package yui.bss.demo.dto;

import lombok.Data;
import yui.bss.demo.vo.SysRoleVo;

/**
 * <p>
 * 系统角色
 * </p>
 *
 * @author yui
 */
@Data
public class SysRoleDto {

    protected SysRoleVo sysRoleVo;

}
