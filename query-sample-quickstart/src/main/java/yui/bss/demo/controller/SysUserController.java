package yui.bss.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.metadata.IPage;

import yui.bss.demo.dto.SysUserDto;
import yui.bss.demo.mgr.SysUserMgr;
import yui.bss.demo.vo.SysUserVo;
import yui.comn.mybatisx.core.conditions.query.FindWrapper;

/**
 * <p>
 * 系统用户
 * </p>
 *
 * @author yui
 */
@RestController
@RequestMapping("/sys/user")
public class SysUserController extends BaseController {
    @Autowired
    private SysUserMgr sysUserMgr;
    
    @GetMapping("page")
    public Object page(String query) { 
        FindWrapper<SysUserVo> fw = getWrapper(query, SysUserVo.class);
        IPage<SysUserDto> page = sysUserMgr.page(fw);
        return build(page, "PAGE"); 
    }
    
    @GetMapping("list")
    public Object list(String query) { 
        FindWrapper<SysUserVo> fw = getWrapper(query, SysUserVo.class);
        List<SysUserDto> list = sysUserMgr.list(fw);
        return build(list, "LIST"); 
    }
    
    @GetMapping("list2")
    public Object list2(String query) { 
        FindWrapper<SysUserVo> fw = getWrapper(query, SysUserVo.class);
        List<SysUserDto> list = sysUserMgr.list(fw);
        return build(list, "LIST2"); 
    }
    
    @GetMapping("listUserARole")
    public Object listUserARole(String query) { 
        FindWrapper<SysUserVo> fw = getWrapper(query, SysUserVo.class);
        List<SysUserDto> list = sysUserMgr.listUserARole(fw);
        return buildList(list, "USER_ROLE"); 
    } 
    
    @GetMapping("listUserARole2")
    public Object listUserARole2(String query) { 
        FindWrapper<SysUserVo> fw = getWrapper(query, SysUserVo.class);
        List<SysUserDto> list = sysUserMgr.listUserARole(fw);
        return buildList(list, "USER_ROLE2"); 
    } 
    
    @GetMapping("listUserLtRole")
    public Object listUserLtRole(String query) { 
        FindWrapper<SysUserVo> fw = getWrapper(query, SysUserVo.class);
        List<SysUserDto> list = sysUserMgr.listUserLtRole(fw);
        return buildList(list, "USER_ROLE"); 
    }   
    
    @GetMapping("listUserWRole")
    public Object listUserWRole(String query) { 
        FindWrapper<SysUserVo> fw = getWrapper(query, SysUserVo.class);
        List<SysUserDto> list = sysUserMgr.listUserWRole(fw);
        return buildList(list, "USER_W_ROLE"); 
    } 
    
    @GetMapping("listUserWRole2")
    public Object listUserWRole2(String query) { 
        FindWrapper<SysUserVo> fw = getWrapper(query, SysUserVo.class);
        List<SysUserDto> list = sysUserMgr.listUserWRole(fw);
        return buildList(list, "USER_W_ROLE2"); 
    }   

}