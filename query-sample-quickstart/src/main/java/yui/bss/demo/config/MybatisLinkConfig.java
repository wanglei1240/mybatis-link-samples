package yui.bss.demo.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.core.injector.ISqlInjector;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;

import yui.comn.mybatisx.extension.injector.SoftSqlInjector;
import yui.comn.mybatisx.extension.plugins.inner.PaginationCacheInterceptor;

/**
 * mybatis配置
 *
 * @author yui
 */
@Configuration
public class MybatisLinkConfig {

    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new PaginationCacheInterceptor(DbType.MYSQL));
        return interceptor;
    }

    @Bean
    public ISqlInjector sqlInjector() {
        return new SoftSqlInjector();
    }
    
}