package yui.bss.demo.mgr;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.toolkit.Constants;

import yui.bss.demo.dto.SysUserDto;
import yui.bss.demo.vo.SysUserVo;
import yui.comn.mybatisx.core.conditions.Wrapper;
import yui.comn.mybatisx.extension.mgr.IMgr;

/**
 * <p>
 * 测试A
 * </p>
 *
 * @author yui
 */
public interface SysUserMgr extends IMgr<SysUserVo, SysUserDto> {

    List<SysUserDto> listUserARole(@Param(Constants.WRAPPER) Wrapper<SysUserVo> wrapper);
    
    List<SysUserDto> listUserLtRole(@Param(Constants.WRAPPER) Wrapper<SysUserVo> wrapper);
    
    List<SysUserDto> listUserWRole(@Param(Constants.WRAPPER) Wrapper<SysUserVo> wrapper);
}
