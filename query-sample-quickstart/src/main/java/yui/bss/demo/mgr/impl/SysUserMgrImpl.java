package yui.bss.demo.mgr.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import yui.bss.demo.dao.SysUserDao;
import yui.bss.demo.dto.SysUserDto;
import yui.bss.demo.mgr.SysUserMgr;
import yui.bss.demo.vo.SysUserVo;
import yui.comn.mybatisx.core.conditions.Wrapper;
import yui.comn.mybatisx.extension.mgr.impl.MgrImpl;

/**
 * <p>
 * 系统用户
 * </p>
 *
 * @author yui
 */
@Service
public class SysUserMgrImpl extends MgrImpl<SysUserDao, SysUserVo, SysUserDto> 
        implements SysUserMgr {

    @Override
    public List<SysUserDto> listUserARole(Wrapper<SysUserVo> wrapper) {
        return baseDao.listUserARole(wrapper);
    }

    @Override
    public List<SysUserDto> listUserLtRole(Wrapper<SysUserVo> wrapper) {
        return baseDao.listUserLtRole(wrapper);
    }

    @Override
    public List<SysUserDto> listUserWRole(Wrapper<SysUserVo> wrapper) {
        return baseDao.listUserWRole(wrapper);
    }

    
    
}
