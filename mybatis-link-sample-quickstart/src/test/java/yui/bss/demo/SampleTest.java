package yui.bss.demo;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import yui.bss.demo.dao.SysUserDao;
import yui.bss.demo.dto.SysUserDto;
import yui.comn.mybatisx.core.conditions.query.FindWrapper;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SampleTest {

    @Autowired
    private SysUserDao sysUserDao;

    @Test
    public void testSelect() {
        System.out.println(("----- mybatis link quickstart ------"));
        List<SysUserDto> list = sysUserDao.listUserARole(new FindWrapper<>());
        System.out.println(list);
    }

}
