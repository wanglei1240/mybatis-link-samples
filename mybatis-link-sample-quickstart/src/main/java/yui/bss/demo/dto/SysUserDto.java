package yui.bss.demo.dto;

import lombok.Data;
import yui.bss.demo.vo.SysRoleVo;
import yui.bss.demo.vo.SysUserVo;

/**
 * <p>
 * 系统用户
 * </p>
 *
 * @author yui
 */
@Data
public class SysUserDto {

    protected SysUserVo sysUserVo;
    protected SysRoleVo sysRoleVo;

}
