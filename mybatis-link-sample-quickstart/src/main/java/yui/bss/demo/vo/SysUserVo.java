package yui.bss.demo.vo;

import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import yui.bss.demo.dao.SysUserDao;
import yui.comn.mybatisx.annotation.DaoDef;

/**
 * <p>
 * 系统用户
 * </p>
 *
 * @author yui
 */
@Data
@TableName("t_sys_user")
@DaoDef(SysUserDao.class)
public class SysUserVo {
    private Long id;
    private Long roleId;
    private String username;
    private String email;
    private String rmks;
    private Integer type;
}
