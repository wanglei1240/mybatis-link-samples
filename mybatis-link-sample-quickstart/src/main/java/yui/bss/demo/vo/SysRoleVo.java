package yui.bss.demo.vo;

import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;

/**
 * <p>
 * 系统角色
 * </p>
 *
 * @author yui
 */
@Data
@TableName("t_sys_role")
// @DaoDef(SysUserDao.class)
public class SysRoleVo {
    private Long id;
    private String cd;
    private String nm;
    private String rmks;
}
