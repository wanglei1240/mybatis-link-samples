package yui.bss.demo.vo;

import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import yui.comn.hub.annotation.Note;

/**
 * <p>
 * 系统角色
 * </p>
 *
 * @author yui
 */
@Data
@TableName("t_sys_role")
public class SysRoleVo {
    
    @Note("ID")
    private Long id;
    @Note("编码")
    private String cd;
    @Note("名称")
    private String nm;
    @Note("备注")
    private String rmks;
}
