package yui.bss.demo.vo;

import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import yui.bss.demo.en.SysUserEn;
import yui.comn.hub.annotation.Note;

/**
 * <p>
 * 系统用户
 * </p>
 *
 * @author yui
 */
@Data
@TableName("t_sys_user")
public class SysUserVo {
    
    @Note("ID")
    private Long id;
    @Note("角色ID")
    private Long roleId;
    @Note("登录名")
    private String username;
    @Note("邮件")
    private String email;
    @Note("备注")
    private String rmks;
    @Note(value="类型（0：管理员，1：非管理员）", enCls = SysUserEn.Type.class)
    private Integer type;
}
