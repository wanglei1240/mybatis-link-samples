package yui.bss.demo.mgr;

import yui.bss.demo.dto.SysUserDto;
import yui.bss.demo.vo.SysUserVo;
import yui.comn.mybatisx.extension.mgr.IMgr;

/**
 * <p>
 * 测试A
 * </p>
 *
 * @author yui
 */
public interface SysUserMgr extends IMgr<SysUserVo, SysUserDto> {

    
    
}
