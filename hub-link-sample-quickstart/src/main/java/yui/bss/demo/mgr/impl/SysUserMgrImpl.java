package yui.bss.demo.mgr.impl;

import org.springframework.stereotype.Service;

import yui.bss.demo.dao.SysUserDao;
import yui.bss.demo.dto.SysUserDto;
import yui.bss.demo.mgr.SysUserMgr;
import yui.bss.demo.vo.SysUserVo;
import yui.comn.mybatisx.extension.mgr.impl.MgrImpl;

/**
 * <p>
 * 系统用户
 * </p>
 *
 * @author yui
 */
@Service
public class SysUserMgrImpl extends MgrImpl<SysUserDao, SysUserVo, SysUserDto> 
        implements SysUserMgr {

    
    
}
