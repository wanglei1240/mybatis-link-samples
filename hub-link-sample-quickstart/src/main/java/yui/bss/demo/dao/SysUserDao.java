package yui.bss.demo.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.toolkit.Constants;

import yui.bss.demo.dto.SysUserDto;
import yui.bss.demo.vo.SysRoleVo;
import yui.bss.demo.vo.SysUserVo;
import yui.comn.mybatisx.annotation.Link;
import yui.comn.mybatisx.annotation.OneToOne;
import yui.comn.mybatisx.core.conditions.Wrapper;
import yui.comn.mybatisx.core.mapper.BaseDao;

/**
 * <p>
 * 系统用户 Mapper 接口
 * </p>
 *
 * @author yui
 */
@Mapper
public interface SysUserDao extends BaseDao<SysUserVo, SysUserDto> {

    @Link( ones = { @OneToOne(leftColumn = "role_id", rightClass = SysRoleVo.class)})
    List<SysUserDto> listUserARole(@Param(Constants.WRAPPER) Wrapper<SysUserVo> wrapper);
    
}
