package yui.bss.demo.en;

import java.io.Serializable;

import yui.comn.hub.model.BaseEnum;


/**
 * <p>
 * 系统用户 枚举类
 * </p>
 *
 * @author yui
 */
public class SysUserEn implements Serializable {
    private static final long serialVersionUID = -97766992214263550L;

    public enum Type implements BaseEnum<Integer> {
	    ADMIN(0, "管理员"),
	    NORMAL_USER(1, "普通用户"),
	    ANNO_USER(2, "匿名用户"),
	    ;
	    private int cd; private String nm;
	    Type (int cd, String nm) { this.cd = cd; this.nm = nm; } public Integer cd() { return cd; } public String nm() { return nm; }
    }
    
}
