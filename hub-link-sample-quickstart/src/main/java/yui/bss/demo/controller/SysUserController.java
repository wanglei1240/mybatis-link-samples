package yui.bss.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import yui.bss.demo.dao.SysUserDao;
import yui.bss.demo.dto.SysUserDto;
import yui.bss.demo.vo.SysUserVo;
import yui.comn.mybatisx.core.conditions.query.FindWrapper;

/**
 * <p>
 * 系统用户
 * </p>
 *
 * @author yui
 */
@RestController
@RequestMapping("/sys/user")
public class SysUserController extends BaseController {
    @Autowired
    private SysUserDao sysUserDao;
    
    @GetMapping("listUserARole")
    public Object listUserARole(String query) { 
        FindWrapper<SysUserVo> fw = getWrapper(query, SysUserVo.class);
        List<SysUserDto> list = sysUserDao.listUserARole(fw);
        return buildList(list, "USER_ROLE"); 
    }     

}