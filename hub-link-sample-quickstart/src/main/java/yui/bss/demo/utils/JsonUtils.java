package yui.bss.demo.utils;

import java.util.Map;

import com.google.gson.Gson;

public class JsonUtils {

	private static Gson gson = new Gson();
	
	@SuppressWarnings("rawtypes")
	public static final Class<Map> DEFAULT_TYPE = Map.class;

	
	public static <T> T JsonStr2JavaBean(String jsonStr, Class<T> toParseClass){
		if (jsonStr == null) {
			return null;
		}
		return gson.fromJson(jsonStr, toParseClass);
	}
}
