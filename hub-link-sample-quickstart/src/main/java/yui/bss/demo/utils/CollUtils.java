/**
* Project: yui3-common-utils
 * Class CollUtils
 * Version 1.0
 * File Created at 2017年10月1日
 * $Id$
 * 
 * Copyright 2010-2015 Yui.com Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Yui Personal. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Yui.com.
 */
package yui.bss.demo.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

/**
 * @author yuyi (1060771195@qq.com)
 */
public class CollUtils {

    /**
     * 判断Collection是否null或者size为0
     * @param c
     * @return
     */
    public static final boolean isEmpty(Collection<?> c) {
        return null == c || 0 == c.size()? true : false;
    }
    
    /**
     * 判断Collection是否不为null或者size不为0
     * @param c
     * @return
     */
    public static final boolean isNotEmpty(Collection<?> c) {
        return !isEmpty(c);
    }
    
    /**
     * 判断指定的一个或者多个集合，是否存在空集合
     * @param colls 参数集合， 一个或者多个
     * @return
     */
    public static final boolean isEmpty(Collection<?>... colls) {
        boolean result = false;
        for (Collection<?> coll : colls) {
            if (null == coll || coll.isEmpty()) {
                result = true;
                break;
            }
        }
        return result;
    }
    
    /**
     * 判断指定的一个或者多个集合，是否都是非空集合
     * @param colls 参数集合， 一个或者多个
     * @return
     */
    public static final boolean isNotEmpty(Collection<?>... colls) {
        return !isEmpty(colls);
    }
    
    /**
     * 获取集合的第一个对象
     * @param c
     * @return
     */
    public static final <T> T get0(Collection<T> c) {
        if (isEmpty(c)) {
            return null;
        }
        return c.iterator().next();
    }
    
    /**
     * 一个集合，拼接成一个带符号的字符串；{"1", "2"} -> ('1', '2')
     * @param c
     * @return
     */
    public static final String toSymbolStr(Collection<String> c) {
        StringBuffer strCond = new StringBuffer("(");
        boolean isFirst = true;
        for (String tmpStatus: c) {
            if (isFirst) {
                strCond.append("'").append(tmpStatus).append("'");
                isFirst = false;
            } else
                strCond.append(",'").append(tmpStatus).append("'");
        }
        strCond.append(")");
        return strCond.toString();
    }
    
    /**
     * String to list 如："1,2,3" -> ("1","2","3")
     * @param args
     * @return
     */
    public static List<Long> toLongList(String args) {
        if (null == args) {
            return null;
        }
        String[] strs = StringUtils.split(args, ",");
        List<Long> longList = new ArrayList<Long>();
        for (String str : strs) {
            longList.add(Long.valueOf(str));
        }
        return longList;
    }
    
    /**
     * String to list 如："1,2,3" -> ("1","2","3")
     * @param args
     * @return
     */
    public static String[] toStrs(String args) {
        if (null == args) {
            return null;
        }
        return StringUtils.split(args, ",");
    }
    
    /**
     * String to list 如："1,2,3" -> ("1","2","3")
     * @param args
     * @return
     */
    public static List<String> toStrList(String args) {
        return toStrList(args, ",");
    }
    
    public static List<String> toStrList(String args, String symbol) {
        if (null == args) {
            return null;
        }
        String[] strs = StringUtils.split(args, symbol);
        List<String> strList = new ArrayList<String>();
        for (String str : strs) {
            strList.add(StringUtils.trim(str));
        }
        return strList;
    }
    
    public static Set<String> toStrSet(String args) {
        List<String> strList = toStrList(args);
        return new HashSet<>(strList);
    }
    
}
